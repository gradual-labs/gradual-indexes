#!/usr/bin/env perl

our $dbug=0;
#--------------------------------
# -- Options parsing ...
#
my $all = 0; 
while (@ARGV && $ARGV[0] =~ m/^-/)
{                                 
  $_ = shift;                     
  #/^-(l|r|i|s)(\d+)/ && (eval "\$$1 = \$2", next); 
  if (/^-v(?:erbose)?/) { $verbose= 1; }
  elsif (/^-a(?:ll)?/) { $all= 1; }
  elsif (/^-y(?:ml)?/) { $yml= 1; }
  elsif (/^-[Qq](?:uiet)?/) { $quiet= 1; }
  else                  { die "Unrecognized switch: $_\n"; }
   
}  
#understand variable=value on the command line...
eval "\$$1='$2'"while $ARGV[0] =~ /^(\w+)=(.*)/ && shift;
#--------------------------------


my $algo=shift;
my $n= ($ARGV[0] =~ m/^\d+$/) ? shift : 1;

my $hdr = {
 'SHA256' => "\x12\x20",
 'SHA1' => "\x11\x14",
 'MD5' => "\xd5\x10"
};

local $/ = undef;
my $buf=<>;
my $hash = &hashr($algo,$n,$buf);
if ($quiet) {
print 'z'. &encode_base58("\x01\x55\x12\x20",$hash);
} else {
printf "h58: z%s\n",&encode_base58("\x01\x55",$hdr->{$algo},$hash);
#printf "hashr: 76'%s\n",&encode_base76($hash);
}

exit $?;
# -----------------------------------------------------------------------
sub hashr {
   my $alg = shift;
   my $rnd = shift; # number of round to run ...
   my $tmp = join('',@_);
   use Crypt::Digest qw();
   my $msg = Crypt::Digest->new($alg) or die $!;
   for (1 .. $rnd) {
      $msg->add($tmp);
      $tmp = $msg->digest();
      $msg->reset;
      #printf "#%d tmp: %s\n",$_,unpack'H*',$tmp;
   }
   return $tmp
}
# ---------------------------------------------------------------------------
sub encode_base58 { # btc
  use Math::BigInt;
  use Encode::Base58::BigInt qw();
  my $bin = join'',@_;
  my $bint = Math::BigInt->from_bytes($bin);
  my $h58 = Encode::Base58::BigInt::encode_base58($bint);
  $h58 =~ tr/a-km-zA-HJ-NP-Z/A-HJ-NP-Za-km-z/;
  return $h58;
}
# ---------------------------------------------------------------------------
1; # $Source: /my/perl/scripts/hashr.pl$
