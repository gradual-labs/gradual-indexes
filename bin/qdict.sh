#

# this script create a default page for the "word" passed 

export PATH=../bin:$PATH
word="${1:-${0##*/}}"


echo "--- # $0"
echo "word: $word"
eval "$(echo $word | perl -S linedigest.pl | eyml)"
echo qm: $qm

cat > word.md <<EOF
# Word $word definition

* definition: [$word](https://duckduckgo.com/?q=!g+define+%22$word%22)
* github: [$word](https://github.com/search?q=%22$word%22)
* youtube: [$word](https://youtube.com/search?q=!g+$word)
* md5'$word:
    [$md5](https://qwant.com/?q=%26g+$md5),
    [$md5z](https://qwant.com/?q=%26g+$md5z)
* sha1'$word:
    [f$sha1](https://qwant.com/?q=%26g+$sha1),
    [$sha1z](https://qwant.com/?q=%26g+$sha1z),
    [$sha1m](https://qwant.com/?q=%26g+$sha1m)
* sha2'$word:
    [$sha2](https://qwant.com/?q=%26g+$sha2),
    [$sha2f](https://qwant.com/?q=%26g+$sha2f),
    [$sha2z](https://qwant.com/?q=%26g+$sha2z)

* immutables:
   - [/ipfs/$qm](https://ipfs.io/ipfs/$qm)
   - [/ipfs/$qm1](https://ipfs.io/ipfs/$qm1)

![bot](https://robohash.org/$qm.png)

EOF
pandoc -t html -f markdown -o index.html word.md
qm=$(ipfs add -Q -n -w word.md index.html)
echo "url: http://localhost:8080/ipfs/$qm"

