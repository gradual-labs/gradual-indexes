## GradualIndexes


gradualIndexes is the entry point to find all ressources avalaible in the gradualVerse

we hve the url for the framapad, framagit, gitlab repositories, as well
as other online assets.


## qPads :

- [qCryption](https://annuel2.framapad.org/p/qCryption)
- [gradualAtoms](https://annuel2.framapad.org/p/gradualAtoms)
- [qPad](https://annuel2.framapad.org/p/qPad) (ether pad/ framapad)
- [gradualGit](https://annuel2.framapad.org/p/gradualGIT)
- [gradualGate](https://annuel2.framapad.org/p/gradualGate)
- [gradualCode](https://annuel2.framapad.org/p/gradualCode) (IRP) (lists; algo, hashes etc.)
- [gradual-kit](https://annuel2.framapad.org/p/gradual-kit) (IPFS mobile/iPad/computer): Package  management i.e. installation of

## gradualGit :

- [gradualCryption](https://framagit.org/gradualverse/holoarchi/holocryption)
- [gradualAtoms](https://framagit.org/gradualsphere/holocore/holoatoms)
- [gradualpad](https://gitlab.com/gradualverse4/holotools/holopad)
- [gradualGit](https://gitlab.com/kinLabs/gradualGIT)
- [gradualGate](https://framagit.org/gradualsphere/hologate)
- [gradualCode](https://gitlab.com/gradualarchi/holocode)
- [gradualKit](https://gitlab.com/gradualkit/holokit.gitlab.io)


## Sharded indexes for qResolver


Indexes are sharded to allow a quick load and distribution w/i the *qiN*etwork

indexes are collected on a use basis,
i.e. all links "clicked" from a search via gradualSearch & gradualCompass



<!--
org: https://github.com/qinLabs
repo: https://gitlab.com/xn--1g8h/overview/-/commits/master

### activities:

- https://framagit.org/dashboard/activity :
  feed: https://framagit.org/dashboard/projects.atom?feed_token=vzHih8Qx1L5kBZ1wSPDm
- https://gihub.com/dashboard/activity
  feed: https://gitlab.com/dashboard/projects.atom?feed_token=GyELSuZa6Y4yDkRrjmCz



### badges:

- http://img.shields.io/... (version, license etc.)
- https://img.shields.io/maintenance/yes/2020.svg
- http://readthedocs.org/projects/gradualtools/badge
- https://git.framasoft.org/spalax/gradualtools/badges/master/coverage.svg
- https://framagit.org/spalax/evariste/builds
-->


