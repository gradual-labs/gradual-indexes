# gradualComponents Resources 

## qCryption
  - framagit : <https://framagit.org/gradualverse/gradualarchi/qcryption>
  - framapad : <https://annuel2.framapad.org/p/qCryption>


## qpad (ether pad/ framapad)
  - gitlab : https://gitlab.com/gradualtools/qpad


##  gradualGit 
  - gitlab : https://gitlab.com/gradual-labs/gradualGIT
  - framapad : https://annuel2.framapad.org/p/gradualGIT


## gradualGate
  - framagit : https://framagit.org/gradual-labs/gradualgate
  - framapad : https://annuel2.framapad.org/p/gradualGate
  - setup (install IPFS/GIT/nextcoud on mobile)  
  - gradualservers (list) (nextcloud, civetweb, netlify's JAMstack, ... heroku etc.)
  - gradualDNS and gradualLNS (Domain and Location Naming Services)
  - gradualgateway ( like https://ipfs.blockringtm.ml ) (ethical gateways)


## gradualCode (IRP) (lists; algo, hashes etc.)
  - gitlab: https://gitlab.com/gradual-labs/gradualcode
  - framapad: https://annuel2.framapad.org/p/gradualCode


## gradualKit (IPFS mobile/iPad/computer): Package  management i.e. installation of gradualSphere to have graceful installation)
  - gitlab : https://gitlab.com/gradual-kit/gradual-kit.gitlab.io
  - framapad: https://annuel2.framapad.org/p/gradual-kit


